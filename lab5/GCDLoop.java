public class GCDLoop {
    public static void main(String[] args) {
        int arg1 = Integer.parseInt(args[0]);
        int arg2 = Integer.parseInt(args[1]);
        int gcd = 1;
        int small;
        if(arg2 < arg1)
            small = arg2;
        else
            small = arg1;
        for(int i=1;i<=small;i++){
            if(arg2 % i == 0 && arg1 % i == 0){
                gcd = i;
            }
        }
        System.out.println(gcd);
    }
}
