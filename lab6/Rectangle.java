public class Rectangle {
    int sideA, sideB;
    Point topLeft;

    public Rectangle(int sideA, int sideB, Point topLeft) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.topLeft = topLeft;
    }
    public int area(){
        return sideA*sideB;
    }
    public int perimeter(){
    return 2*(sideB+sideA);

    }
    public Point[] corners(){
        Point[] corners=new Point[4];
        corners[0]=topLeft;
        corners[1]=new Point(topLeft.xCoord+sideA,topLeft.yCoord);
        corners[2]=new Point(topLeft.xCoord,sideB+topLeft.yCoord);
        corners[3]=new Point(topLeft.xCoord+sideA,topLeft.yCoord+sideB);

        return corners;
    }
}
